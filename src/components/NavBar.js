import { Fragment } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';

export default function AppNavBar(){
    const email = localStorage.getItem(`email`)
    console.log(email)
    return(
        <Navbar bg="primary" expand="lg">
          <Container>
            <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="/" className="text-light">Home</Nav.Link>
                <Nav.Link href="/course" className="text-light">Course</Nav.Link>
              
                {
                  email !== null
                   ? 
                   <Nav.Link href="/logout" className="text-light">Logout</Nav.Link>
                   :
                   <Fragment>
                      <Nav.Link href="/login" className="text-light">Login</Nav.Link>
                      <Nav.Link href="/register" className="text-light">Register</Nav.Link>
                   </Fragment>
                }


                
              </Nav>
    
            </Navbar.Collapse>
          </Container>
        </Navbar> 
    )
}