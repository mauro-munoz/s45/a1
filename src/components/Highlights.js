import React, {Fragment} from 'react'
import { Card, Button,Row,Col } from 'react-bootstrap';

export default function Highlights() {
  return (
<Fragment>
<Row className="my-3 mx-3">
    <Col xs={12} md={4}>
        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>Learn From Home</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
            </Card.Body>
        </Card>
    </Col>
    <Col xs={12} md={4}>
        <Card style={{ width: '18rem' }}> 
            <Card.Body>
                <Card.Title>Study now pay later</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
            </Card.Body>
        </Card>
    </Col>
    <Col xs={12} md={4}>
        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>Be part of our community</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
            </Card.Body>
        </Card>
    </Col>
</Row>
</Fragment>
  )
}
