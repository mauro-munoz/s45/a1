import { Card, Button } from 'react-bootstrap';
import { useState} from 'react';


export default function CourseCard({courseProp}){
  // count is the variable, setCount is the function
  let [count, setCount] = useState(30)

  const {CourseName,description,price} = courseProp

  const handleClick = () => {
    if(count>0){
      setCount(count-1)
    }else{
      alert(`No more seats available`)
    }
  }

    return(
        <Card style={{ width: '18rem' }} className="my-5 mx-3">
            <Card.Body>
              <Card.Title><h3>{CourseName}</h3></Card.Title>
              <Card.Text>
                <Card.Text>Description:</Card.Text>
                <Card.Subtitle>{description}</Card.Subtitle>
                <Card.Text>Price:</Card.Text>
                <Card.Subtitle>{price}</Card.Subtitle>
                <Card.Text>Seats Available: {count}</Card.Text>
              </Card.Text>
              <Button variant="info" onClick={handleClick}>Enroll Now</Button>
            </Card.Body>
      </Card>
    )
}