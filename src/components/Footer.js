
export default function Footer(){
    return(
        <div className="bg-info d-flex justify-content-center align-items-center text-white" style={{height: '5vh'}}>
          <p className="m-0 font-weight-bold">Mauro Munoz &#64; Course Booking | Zuitt Coding Bootcamp &#169;</p>
        </div>
    )
}