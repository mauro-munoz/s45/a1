import React,{useState} from "react";
import Home from "./pages/Home";
import Course from "./pages/Course"
import Register from "./pages/Register"
import Login from "./pages/Login"
import Logout from "./pages/Logout"
import NotFound from "./pages/NotFound"
//components
import Navbar from "./components/NavBar"
import Footer from "./components/Footer"

//router
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

//context
import{UserProvider} from "./UserContext"

function App() {

  const [user,setUser] = useState({
    id: 123,
    isAdmin: true
  })

  return(
    <UserProvider value={{user, setUser}}>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/course" element={<Course/>} />
          <Route path="/register" element={<Register/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/logout" element={<Logout/>}/>
          <Route path="*" element={<NotFound/>}/>
        </Routes>
        <Footer />
      </BrowserRouter>
    </UserProvider>   
  )
}

export default App;

