import React from 'react'

export default function Logout() {
    localStorage.clear()
  return (
    <h1>You have Logged Out</h1>
  )
}
