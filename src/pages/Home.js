// import NavBar from "../components/NavBar";
import Banner from "../components/Banner";

// import Footer from "../components/Footer";
import Hightlights from "../components/Highlights";


export default function Home(){
    return(
        <>
            
            <Banner/>
            <Hightlights />
        </>
    )
}