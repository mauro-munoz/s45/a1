import React, {useEffect, useState} from 'react'
import {Form, Button,Row,Col,Container} from 'react-bootstrap'
import {useNavigate} from "react-router-dom"
export default function Login() {
    const navigate = useNavigate()
    let [email, setemail]= useState("")
    let [password, setpassword]= useState("")
   
    let [isDisabled, setisDisabled]=useState(true)
    // useEffect(function,options)
    useEffect(()=>{console.log(email)},[email])
    useEffect(()=>{console.log(password)},[password])
    useEffect(()=>{
        if( email !== "" && password !== "" ){
            setisDisabled(false)
        }
    })
    const registerUser = (e) => {
        e.preventDefault()

        //post to API
        fetch(`https://zuitt-capstone2-mauro.herokuapp.com/users/login`,{
            method:`POST`,
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(result => result.json().then(result =>{

            // localStorage.setItem(`id`,result._id),
            // localStorage.setItem(`admin`,result.isAdmin),
            // localStorage.setItem(`fullName`,result.fullName)
            // console.log(result._id, result.isAdmin, result.fullName)
            if(result.message == `User does not exist`){
                alert(`Something went wrong: ${result.message}`)
            }else {
                // console.log(result)
                localStorage.setItem('token', result.message)
                localStorage.setItem('email', email)
                // console.log(email)
                alert(`User successfully logged in`)
                setemail("")
                setpassword("")
                navigate(`/course`)}
        
           }
            ))
    }

  return (
      <Container className="m-5">
        <h2 className ="d-flex justify-content-center">Login</h2>
          <Row className ="justify-content-center">
          <Col xs={12} md={6}>
            <Form className="m-4" onSubmit={(e)=>registerUser(e)}>


                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value ={email}
                    onChange={(e)=> setemail (e.target.value)} />
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} 
                        onChange={(e)=> setpassword (e.target.value)}
                    />
                </Form.Group>

                <Button  variant="info" type="submit" disabled={isDisabled}>
                    Submit
                </Button>

            </Form>
          </Col>
      </Row>
      </Container>
  )
}
