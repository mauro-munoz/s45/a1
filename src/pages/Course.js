import React,{Fragment, useContext} from 'react'
import CourseCard from './../components/CourseCard'
import courses from "./../mockdata/courses"
import Highlights from "./../components/Highlights"
import Banner from "./../components/Banner"
import UserContext from "./../UserContext"
import {useNavigate} from "react-router-dom"




export default function Course() {
    // console.log(courses[0])
    const navigate = useNavigate()
    const {user} = useContext(UserContext)
    console.log(user)
    const coursesCard = courses.map(card=>{
        return <CourseCard key={card.id} courseProp={card}/>
    })
    if(user !== null){
      return (
        <Fragment>
          <Banner />
          <Highlights />
          {coursesCard}
        </Fragment>
    )
    }else{
      return navigate("/course")
    }
 
}
